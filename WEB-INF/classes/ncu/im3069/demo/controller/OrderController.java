package ncu.im3069.demo.controller;

import java.io.IOException;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.*;
import javax.servlet.http.*;
import org.json.*;

import ncu.im3069.demo.app.Member;
import ncu.im3069.demo.app.MemberHelper;
import ncu.im3069.demo.app.Order;
import ncu.im3069.demo.app.TicketHelper;
import ncu.im3069.demo.app.OrderHelper;
import ncu.im3069.demo.app.Ticket;
import ncu.im3069.tools.JsonReader;

import javax.servlet.annotation.WebServlet;

@WebServlet("/api/order.do")
public class OrderController extends HttpServlet {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

    private TicketHelper th =  TicketHelper.getHelper();

	private OrderHelper oh =  OrderHelper.getHelper();

    public OrderController() {
        super();
    }

   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  
        JsonReader jsr = new JsonReader(request);

        String temp_member_id = jsr.getParameter("member_id");
        
        int member_id = Integer.parseInt(temp_member_id);

        JSONObject resp = new JSONObject();

       
        JSONObject query = oh.getAll(member_id);
        resp.put("status", "200");
        resp.put("message", "所有訂單資料取得成功");
        resp.put("response", query);

        jsr.response(resp, response);
	}

  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        JsonReader jsr = new JsonReader(request);
        JSONObject jso = jsr.getObject();
        String temp_member_id = jso.getString("member_id");       
        int member_id = Integer.parseInt(temp_member_id);
        System.out.println(member_id);
        String address = jso.getString("address");
        String phone = jso.getString("phone");
        String payment_method = jso.getString("payment_method");      
        JSONArray item = jso.getJSONArray("item");
        //System.out.print(address);

        Order od = new Order( member_id,address, phone, payment_method);

        for(int i=0 ; i < item.length() ; i++) {
        	
        	JSONObject jsonObject1 = item.getJSONObject(i);
            String ticket_title = jsonObject1.optString("title");
            String ticket_seat = jsonObject1.optString("seat");
            Ticket tempticket = th.getTicketID(ticket_title, ticket_seat);
            int ticketid = tempticket.getID();
            String ticket_id = Integer.toString(ticketid);
            Ticket ticket = th.getById(ticket_id);
            od.addOrderTicket(ticket);
        }

        JSONObject result = oh.create(od);
        //od.setId((int) result.getInt("order_id"));
        //od.setOrderTicketID(result.getJSONArray("order_ticket_id"));

        JSONObject resp = new JSONObject();
        resp.put("status", "200");
        resp.put("message", "訂單新增成功！");
        resp.put("response", od.getOrderAllInfo());

        jsr.response(resp, response);
	}
	
	public void doDelete(HttpServletRequest request, HttpServletResponse response)
	        throws ServletException, IOException {
	        /** 透過JsonReader類別將Request之JSON格式資料解析並取回 */
	        JsonReader jsr = new JsonReader(request);
	        JSONObject jso = jsr.getObject();
	        
	        /** 取出經解析到JSONObject之Request參數 */
	        int id = jso.getInt("id");
	        
	        /** 透過MemberHelper物件的deleteByID()方法至資料庫刪除該名會員，回傳之資料為JSONObject物件 */
	        JSONObject query = oh.deleteByID(id);
	        
	        /** 新建一個JSONObject用於將回傳之資料進行封裝 */
	        JSONObject resp = new JSONObject();
	        resp.put("status", "200");
	        resp.put("message", "訂單移除成功！");
	        resp.put("response", query);

	        /** 透過JsonReader物件回傳到前端（以JSONObject方式） */
	        jsr.response(resp, response);
	    }

}
