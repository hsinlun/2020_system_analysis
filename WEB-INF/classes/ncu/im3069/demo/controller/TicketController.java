package ncu.im3069.demo.controller;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.*;
import javax.servlet.http.*;
import org.json.*;


import ncu.im3069.demo.app.Ticket;
import ncu.im3069.demo.app.TicketHelper;
import ncu.im3069.tools.JsonReader;

@WebServlet("/api/ticket.do")
public class TicketController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private TicketHelper th =  TicketHelper.getHelper();

    public TicketController() {
        super();
        // TODO Auto-generated constructor stub
    }

    //有訂單就代表有票券
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/** 透過JsonReader類別將Request之JSON格式資料解析並取回 */
        JsonReader jsr = new JsonReader(request);
        /** 若直接透過前端AJAX之data以key=value之字串方式進行傳遞參數，可以直接由此方法取回資料 */
        //String order_list = jsr.getParameter("order_list");
        String ac_title = jsr.getParameter("ac_title");
        String seat_name = jsr.getParameter("seat_name");
        //System.out.println(ac_title);
        //System.out.println(seat_name);
        
        System.out.println("title: "+ac_title+", seat: "+seat_name);

        JSONObject resp = new JSONObject();
        /** 判斷該字串是否存在，若存在代表要取回購物車內產品之資料，否則代表要取回全部資料庫內產品之資料 */
        if (!ac_title.isEmpty() && seat_name.isEmpty()) {
        	JSONObject query = th.checkDuplicate(ac_title);
          //JSONObject query = th.getByActiIDandSeatName(acti_id, seat_name);
        	//boolean isBooked = th.getByActiIDandSeatName(acti_id, seat_name);
        	//boolean isBooked = th.getByTitleandSeatName(acti_title, seat_name);

          resp.put("status", "200");
          resp.put("message", "回傳訂位狀況");
          resp.put("response", query); 

        //若回傳tile和seat
        }else if(!ac_title.isEmpty() && !seat_name.isEmpty()) {
        	th.deleteById(ac_title, seat_name);
        	
            resp.put("status", "200");
            resp.put("message", "已刪除票券");
            //resp.put("response", query);
        }
        //沒有order就沒有ticket
        else {
          resp.put("message","查無票券資料");
        }

        jsr.response(resp, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  // TODO Auto-generated method stub
		  JsonReader jsr = new JsonReader(request);
		        JSONObject jso = jsr.getObject();
		        
		        /** 取出經解析到JSONObject之Request參數 */
		        String title = jso.getString("title");
		        String artist = jso.getString("artist");
		        int price = jso.getInt("price");
		        String seat = jso.getString("seat");
		        String start_time = jso.getString("start_time");
		        String end_time = jso.getString("end_time");
		        
		        //System.out.println("checkDup: "+th.checkDuplicateSeat(title,seat));
		        //JSONObject test = th.checkDuplicateSeat(title,seat);
		        //System.out.println("data: "+test.getString("data"));
		        //System.out.println("title: "+title+", artist: "+artist+", price: "+price+", seat: "+seat+", start_time: "+start_time+", end_time: "+end_time);
		        
		        /** 建立一個新的會員物件 */
		        Ticket t = new Ticket(title,artist,price,seat,start_time,end_time);
		        
		        /** 後端檢查是否有欄位為空值，若有則回傳錯誤訊息 */
		        if(title.isEmpty() || artist.isEmpty() ||seat.isEmpty()||start_time.isEmpty()|| end_time.isEmpty()) {
		            /** 以字串組出JSON格式之資料 */
		            String resp = "{\"status\": \'400\', \"message\": \'欄位不能有空值\', \'response\': \'\'}";
		            /** 透過JsonReader物件回傳到前端（以字串方式） */
		            jsr.response(resp, response);
		        }
		        /** 透過MemberHelper物件的checkDuplicate()檢查該會員電子郵件信箱是否有重複 */
		        else if (th.checkDuplicateSeat(title,seat) == 0) {
		            /** 透過MemberHelper物件的create()方法新建一個會員至資料庫 */
		            JSONObject data = th.create(t);
		            
		            /** 新建一個JSONObject用於將回傳之資料進行封裝 */
		            JSONObject resp = new JSONObject();
		            resp.put("status", "200");
		            resp.put("message", "成功!已儲存票券");
		            resp.put("response", data);
		            
		            /** 透過JsonReader物件回傳到前端（以JSONObject方式） */
		            jsr.response(resp, response);
		        }
//		        else if (test.get("data")==null) {
//		            /** 透過MemberHelper物件的create()方法新建一個會員至資料庫 */
//		            JSONObject data = th.create(t);
//		            
//		            /** 新建一個JSONObject用於將回傳之資料進行封裝 */
//		            JSONObject resp = new JSONObject();
//		            resp.put("status", "200");
//		            resp.put("message", "成功!已儲存票券");
//		            resp.put("response", data);
//		            
//		            /** 透過JsonReader物件回傳到前端（以JSONObject方式） */
//		            jsr.response(resp, response);
//		        }
		        else {
		            /** 以字串組出JSON格式之資料 */
		            String resp = "{\"status\": \'400\', \"message\": \'此座位已被預訂，請選擇其他座位！\', \'response\': \'\'}";
		            /** 透過JsonReader物件回傳到前端（以字串方式） */
		            jsr.response(resp, response);
		        }
	}

}
