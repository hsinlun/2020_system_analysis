package ncu.im3069.demo.controller;

import java.io.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;
import ncu.im3069.demo.app.Member;
import ncu.im3069.demo.app.MemberHelper;
import ncu.im3069.tools.JsonReader;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * The Class MemberController<br>
 * MemberController類別（class）主要用於處理Member相關之Http請求（Request），繼承HttpServlet
 * </p>
 * 
 * @author IPLab
 * @version 1.0.0
 * @since 1.0.0
 */

public class MemberController extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    
    private MemberHelper mh =  MemberHelper.getHelper();
    
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
       
        JsonReader jsr = new JsonReader(request);
        JSONObject jso = jsr.getObject();

        String name = jso.getString("name");
        String email = jso.getString("email");
        String passwd = jso.getString("passwd");      
        String dobstring = jso.getString("dob");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date dobutil = null;
		try {
			dobutil = sdf.parse(dobstring);
		} catch (ParseException e) {
			e.printStackTrace();
		}
        java.sql.Date dob = new Date(dobutil.getTime());
 
        Member m = new Member(name,email, passwd,  dob);
        
        if(email.isEmpty() || passwd.isEmpty() || name.isEmpty()|| dobstring.isEmpty()) {
  
            String resp = "{\"status\": \'400\', \"message\": \'欄位不能有空值\', \'response\': \'\'}";
        
            jsr.response(resp, response);
        }
        
        else if (!mh.checkDuplicate(m)) {
  
            JSONObject data = mh.create(m);
            JSONObject resp = new JSONObject();
            resp.put("status", "200");
            resp.put("message", "成功! 註冊會員資料...");
            resp.put("response", data);
            
            jsr.response(resp, response);
        }
        else {
            String resp = "{\"status\": \'400\', \"message\": \'新增帳號失敗，此E-Mail帳號重複！\', \'response\': \'\'}";
            jsr.response(resp, response);
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    	
        JsonReader jsr = new JsonReader(request);
        String email = jsr.getParameter("email");
        String passwd = jsr.getParameter("passwd");
        Member m = new Member(email, passwd);
              
        if(!email.isBlank()&&!passwd.isBlank()&&mh.checklogin(m)) {
           
            JSONObject query = mh.login(email, passwd);
            
            JSONObject resp = new JSONObject();
            resp.put("status", "200");
            resp.put("message", "登入成功");
            resp.put("response", query);

            jsr.response(resp, response);
        }
    }

    
    
    public void doPut(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
       
        JsonReader jsr = new JsonReader(request);
        JSONObject jso = jsr.getObject();

        String temp_ID = jso.getString("ID");
        int ID = Integer.parseInt(temp_ID);
        String name = jso.getString("name");
        String email = jso.getString("email");
        String passwd = jso.getString("passwd");      
        String dobstring = jso.getString("dob");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date dobutil = null;
		try {
			dobutil = sdf.parse(dobstring);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        java.sql.Date dob = new Date(dobutil.getTime());

        Member m = new Member(ID, name, email, passwd,dob);
        
        JSONObject data = m.update();
        
        JSONObject resp = new JSONObject();
        resp.put("status", "200");
        resp.put("message", "成功! 更新會員資料...");
        resp.put("response", data);

        jsr.response(resp, response);
    }
}