package ncu.im3069.demo.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.annotation.WebServlet;
import javax.servlet.*;
import javax.servlet.http.*;
import org.json.*;
import java.sql.Date;

import ncu.im3069.demo.app.ActivityHelper;
import ncu.im3069.demo.app.Activity;
import ncu.im3069.tools.JsonReader;

@WebServlet("/api/activity.do")
public class ActivityController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ActivityHelper ach =  ActivityHelper.getHelper();

    public ActivityController() {
        super();
        // TODO Auto-generated constructor stub
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	  
        JsonReader jsr = new JsonReader(request);
        
        String  id= jsr.getParameter("id");
        String keyword = jsr.getParameter("keyword");
        JSONObject resp = new JSONObject();
        JSONObject query;
        if(id.isEmpty()&& keyword.isEmpty()) {
          // 取出所有活動資料
            query = ach.getAll();
            //System.out.print(query);

            resp.put("status", "200");
            resp.put("message", "所有活動資料取得成功");
            resp.put("response", query);
        }else {
        	if (!id.isEmpty() && keyword.isEmpty()) {
        		//單筆活動詳細資料
        		query = ach.getById(id);
        		
            	resp.put("status", "200");
                resp.put("message", "單筆活動資料取得成功");
                resp.put("response", query);
                
        	}else if(id.isEmpty() && !keyword.isEmpty()){
        		query = ach.getByKeyword(keyword);
        		System.out.println(query);
        		
            	resp.put("status", "200");
                resp.put("message", "回傳搜尋活動資料");
                resp.put("response", query);
        	}

        }
        jsr.response(resp, response);
        


 }// end do get

	protected void doPost(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//System.out.print("yeah!");
		doGet(request, response);
	}

	
}
