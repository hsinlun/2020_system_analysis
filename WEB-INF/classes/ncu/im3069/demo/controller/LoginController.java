package ncu.im3069.demo.controller;

import java.io.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import org.json.*;
import ncu.im3069.demo.app.Member;
import ncu.im3069.demo.app.MemberHelper;
import ncu.im3069.tools.JsonReader;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * The Class MemberController<br>
 * MemberController類別（class）主要用於處理Member相關之Http請求（Request），繼承HttpServlet
 * </p>
 * 
 * @author IPLab
 * @version 1.0.0
 * @since 1.0.0
 */
@WebServlet("/api/login.do")
public class LoginController extends HttpServlet {
    
    private static final long serialVersionUID = 1L;
    
    private MemberHelper mh =  MemberHelper.getHelper();
    
   
    
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    	
        JsonReader jsr = new JsonReader(request);
        JSONObject jso = jsr.getObject();
        String email = jso.getString("email");
        String passwd = jso.getString("passwd");
        Member m = new Member(email, passwd);
              
        if(!email.isBlank()&&!passwd.isBlank()&&mh.checklogin(m)) {
           
            JSONObject query = mh.login(email, passwd);           
            JSONObject resp = new JSONObject();
            resp.put("status", "200");
            resp.put("message", "登入成功");
            resp.put("response", query);

            jsr.response(resp, response);
        }
        else {
            String resp = "{\"status\": \'400\', \"message\": \'登入錯誤\', \'response\': \'\'}";
            jsr.response(resp, response);
        }
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         
            JsonReader jsr = new JsonReader(request);
            String temp_id = jsr.getParameter("member_id");
            int ID = Integer.parseInt(temp_id);
            
           
            JSONObject query = mh.getByID(ID);
                              
            JSONObject resp = new JSONObject();
            resp.put("status", "200");
            resp.put("message", "會員資料取得成功");
            resp.put("response", query);
            jsr.response(resp, response);
            
        }

    
   
}