package ncu.im3069.demo.app;

import org.json.*;

import java.sql.Date;

public class Activity {

    private int ID;
    private String title;
    private String image;
	private String description;
	private String artist;
	//private Date start_time;
	private String start_time;
	private String end_time;
	
	private ActivityHelper ach = ActivityHelper.getHelper();
   
	public Activity(int ID) {
		this.ID =ID;
	}

	public Activity(int ID, String title, String artist, String description,  String image, String start_time, String end_time) {
		this.ID = ID;
		this.title = title;
		this.artist = artist;
		this.description = description;
		this.image = image;
		this.start_time=start_time;
		this.end_time=end_time;
	}

    //ID
	public int getID() {
		return this.ID;
	}

    //活動標題
	public String getTitle() {
		return this.title;
	}
	
	//圖片
	public String getImage() {
		return this.image;
	}
    //活動描述
	public String getDescription() {
		return this.description;
	}

	//銵冽���
	public String getArtist() {
		return this.artist;
	}
	//開始時間
	public String getStarttime() {
		return this.start_time;
	}
	//結束時間
	public String getEndtime() {
		return this.end_time;
	}

	
	public JSONObject getData() {
        
        JSONObject jso = new JSONObject();
        jso.put("id", getID());
        jso.put("title", getTitle());
        jso.put("artist", getArtist());
        jso.put("description", getDescription());
        jso.put("image", getImage());       
        jso.put("start_time", getStarttime());
        jso.put("end_time", getEndtime());

        return jso;
    }
}
