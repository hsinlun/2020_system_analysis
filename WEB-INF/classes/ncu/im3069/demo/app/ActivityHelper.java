package ncu.im3069.demo.app;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.TimeZone;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.json.*;

import ncu.im3069.demo.util.DBMgr;
import ncu.im3069.demo.app.Activity;

public class ActivityHelper {
	
    private ActivityHelper() {
        
    }
    
    private static ActivityHelper ach;
    private Connection conn = null;
    private PreparedStatement pres = null;
    
    public static ActivityHelper getHelper() {
        
        if(ach == null) ach = new ActivityHelper();
        
        return ach;
    }
  
    public JSONObject getAll() {//取出全部activity
        
    	Activity ac = null;
        
        JSONArray jsa = new JSONArray();
        
        String exexcute_sql = "";
       
        long starts_time = System.nanoTime();
        
        int row = 0;
        
        ResultSet rs = null;
        
        try {
            
            conn = DBMgr.getConnection();
            
            String sql = "SELECT * FROM `ncutix`.`activity`";
            
            
            pres = conn.prepareStatement(sql);
            
            rs = pres.executeQuery();

            
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            
            while(rs.next()) {
                
                row += 1;
                
               
                int activity_ID = rs.getInt("ID");
                String title = rs.getString("title");
                String image = rs.getString("image");
                String description = rs.getString("description");
                String artist = rs.getString("artist");
                Timestamp start_time2 = rs.getTimestamp("start_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));//通過資料庫訪問獲取到該資料
                Timestamp end_time2 = rs.getTimestamp("end_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));//通過資料庫訪問獲取到該資料
                String start_time = start_time2.toString();
                String end_time = end_time2.toString();
                ac = new Activity(activity_ID, title,artist, description, image, start_time, end_time);

                
                jsa.put(ac.getData());
            }

        } catch (SQLException e) {
           
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            
            e.printStackTrace();
        } finally {
            
            DBMgr.close(rs, pres, conn);
        }
        
        long ends_time = System.nanoTime();
       
        long duration = (ends_time - starts_time);
        
        
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }
    
    public JSONObject getByIdList(String data) {//照ID排序取出
      
      Activity ac = null;
     
      JSONArray jsa = new JSONArray();
      
      String exexcute_sql = "";
      
      long starts_time = System.nanoTime();
     
      int row = 0;
    
      ResultSet rs = null;

      try {
         
          conn = DBMgr.getConnection();
          String[] in_para = DBMgr.stringToArray(data, ",");
         
          String sql = "SELECT * FROM `ncutix`.`activity` WHERE `activity`.`ID`";
          for (int i=0 ; i < in_para.length ; i++) {
              sql += (i == 0) ? "in (?" : ", ?";
              sql += (i == in_para.length-1) ? ")" : "";
          }
          
         
          pres = conn.prepareStatement(sql);
          for (int i=0 ; i < in_para.length ; i++) {
            pres.setString(i+1, in_para[i]);
          }
         
          rs = pres.executeQuery();

         
          exexcute_sql = pres.toString();
          System.out.println(exexcute_sql);
          
          
          while(rs.next()) {
              
              row += 1;
              
              int activity_ID = rs.getInt("ID");
              String title = rs.getString("title");
              String image = rs.getString("image");
              String description = rs.getString("description");
              String artist = rs.getString("artist");
              Timestamp start_time2 = rs.getTimestamp("start_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
              Timestamp end_time2 = rs.getTimestamp("end_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
              String start_time = start_time2.toString();
              String end_time = end_time2.toString();
            
              ac = new Activity(activity_ID, title,artist, description, image, start_time, end_time);
            
              jsa.put(ac.getData());
          }

      } catch (SQLException e) {
          
          System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
      } catch (Exception e) {
         
          e.printStackTrace();
      } finally {
          
          DBMgr.close(rs, pres, conn);
      }
      
     
      long ends_time = System.nanoTime();
     
      long duration = (ends_time - starts_time);
      
     
      JSONObject response = new JSONObject();
      response.put("sql", exexcute_sql);
      response.put("row", row);
      response.put("time", duration);
      response.put("data", jsa);

      return response;
  }
    public JSONObject getByKeyword(String keyword) {//照ID排序取出
        
        Activity ac = null;
       
        JSONArray jsa = new JSONArray();
        
        String exexcute_sql = "";
        
        long starts_time = System.nanoTime();
       
        int row = 0;
      
        ResultSet rs = null;

        try {
           
            conn = DBMgr.getConnection();
            
           
            String sql = "SELECT * FROM `ncutix`.`activity` WHERE `activity`.`title`like '%"+ keyword +"%'";
//            String sql = "SELECT * FROM `ncutix`.`activity` WHERE `activity`.`title`like '% ? %'";
            
           
            pres = conn.prepareStatement(sql);
            //pres.setString(1,keyword);
            System.out.println(sql);
           
            rs = pres.executeQuery();

           
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            
            while(rs.next()) {
                
                row += 1;
                
                int activity_ID = rs.getInt("ID");
                String title = rs.getString("title");
                String image = rs.getString("image");
                String description = rs.getString("description");
                String artist = rs.getString("artist");
                Timestamp start_time2 = rs.getTimestamp("start_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
                Timestamp end_time2 = rs.getTimestamp("end_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
                String start_time = start_time2.toString();
                String end_time = end_time2.toString();
              
                ac = new Activity(activity_ID, title,artist, description, image, start_time, end_time);
              
                jsa.put(ac.getData());
            }

        } catch (SQLException e) {
            
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
           
            e.printStackTrace();
        } finally {
            
            DBMgr.close(rs, pres, conn);
        }
        
       
        long ends_time = System.nanoTime();
       
        long duration = (ends_time - starts_time);
        
       
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }// end getByKeyword
    
    public JSONObject getById(String ID) {
        
        Activity ac = null;
           
           JSONObject jo = new JSONObject();
           
           String exexcute_sql = "";
          
           long starts_time = System.nanoTime();
           
           int row = 0;
           
           ResultSet rs = null;
           
           try {
               
               conn = DBMgr.getConnection();
               
               String sql = "SELECT * FROM `ncutix`.`activity` where `activity`.`ID`=?";
               
               
               pres = conn.prepareStatement(sql);
               pres.setString(1, ID);
               rs = pres.executeQuery();

               
               exexcute_sql = pres.toString();
               System.out.println(exexcute_sql);
               
               
               while(rs.next()) {
                   
                   row += 1;
                   
                  
                   int activity_ID = rs.getInt("ID");
                   String title = rs.getString("title");
                   String image = rs.getString("image");
                   String description = rs.getString("description");
                   String artist = rs.getString("artist");
                 
                   Timestamp start_time2 = rs.getTimestamp("start_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));//通過資料庫訪問獲取到該資料      
                   Timestamp end_time2 = rs.getTimestamp("end_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
                   String start_time = start_time2.toString();
                   String end_time = end_time2.toString();
                   ac = new Activity(activity_ID, title,artist, description, image, start_time, end_time);
                   
                   //jo.put(ID,ac.getData());
                   jo=ac.getData();
                   
               }

           } catch (SQLException e) {
              
               System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
           } catch (Exception e) {
               
               e.printStackTrace();
           } finally {
               
               DBMgr.close(rs, pres, conn);
           }
           
           long ends_time = System.nanoTime();
          
           long duration = (ends_time - starts_time);
           
           
           JSONObject response = new JSONObject();
           response.put("sql", exexcute_sql);
           response.put("row", row);
           response.put("time", duration);
           response.put("data", jo);

           return response;
       }

}
