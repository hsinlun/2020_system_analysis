package ncu.im3069.demo.app;

import java.sql.Date;
import ncu.im3069.demo.app.*;

import org.json.*;

public class Ticket {

    private int ID;
    private String title;
    private String artist;
    private int price;
	private String seat;
	private String start_time;
	private String end_time;
	
	
	//private Activity ac;
	//private Seatlist sl;
    /**
     * 實例化（Instantiates）一個新的（new）Product 物件<br>
     * 採用多載（overload）方法進行，此建構子用於新增產品時
     *
     * @param id 產品編號
     */
	public Ticket(String title, String artist, int price,String seat, String start_time,String end_time) {
		  this.title = title;
		  this.artist = artist;
		  this.price = price;
		  this.seat = seat;
		  this.start_time = start_time;
		  this.end_time = end_time;
		 }
	public Ticket(int ID, String title, String artist, int price,String seat, String start_time,String end_time) {
		  this.ID = ID;
		  this.title = title;
		  this.artist = artist;
		  this.price = price;
		  this.seat = seat;
		  this.start_time = start_time;
		  this.end_time = end_time;
		 }

	public int getID() {
		return this.ID;
	}
	public String getTitle() {
		return this.title;
	}
	public String getArtist() {
		return this.artist;
	}
	public int getPrice() {
		return this.price;	
	}
	public String getSeat() {
		return this.seat;
	}
	public String getStarttime() {
		return this.start_time;
	}
	public String getEndtime() {
		return this.end_time;
	}
    /**
     * 取得產品資訊
     *
     * @return JSONObject 回傳產品資訊
     */
	public JSONObject getData() {
        /** 透過JSONObject將該項產品所需之資料全部進行封裝*/
        JSONObject jso = new JSONObject();
        jso.put("ID", getID());
        jso.put("title", getTitle());
        jso.put("artist", getArtist());
        jso.put("price", getPrice());
        jso.put("seat", getSeat());
        jso.put("start_time", getStarttime());
        jso.put("end_time", getEndtime());

        return jso;
    }
}
