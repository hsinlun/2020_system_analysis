package ncu.im3069.demo.app;

import org.json.JSONObject;
import ncu.im3069.demo.util.Arith;

public class OrderItem {

    private int ID;

    private Ticket ticket;

    private int price;

    private TicketHelper ph =  TicketHelper.getHelper();

    public OrderItem(Ticket ticket) {//建立新訂單細項
        this.ticket = ticket;
        this.price = this.ticket.getPrice();
    }


    public OrderItem(int ID, int order_id, int ticket_id, int price) { //修改訂單細項
        this.ID = ID;
        this.price = price;
        getTicketFromDB(ticket_id);
    }


    private void getTicketFromDB(int ticket_id) {
        String ID = String.valueOf(ticket_id);
        this.ticket = ph.getById(ID);
    }

    public Ticket getTicket() {
        return this.ticket;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
    
    public int getID() {
        return this.ID;
    }

    
    public int getPrice() {
        return this.price;
    }

  
    public JSONObject getData() {
        JSONObject data = new JSONObject();
        data.put("ID", getID());
        data.put("ticket", getTicket().getData());
        data.put("price", getPrice());

        return data;
    }

	
}
