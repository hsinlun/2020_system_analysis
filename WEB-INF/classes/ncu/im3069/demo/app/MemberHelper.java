package ncu.im3069.demo.app;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.TimeZone;

import org.json.*;

import ncu.im3069.demo.util.DBMgr;

// TODO: Auto-generated Javadoc
/**
 * <p>
 * The Class MemberHelper<br>
 * MemberHelper類別（class）主要管理所有與Member相關與資料庫之方法（method）
 * </p>
 * 
 * @author IPLab
 * @version 1.0.0
 * @since 1.0.0
 */

public class MemberHelper {
    
    private MemberHelper() { }
    
    private static MemberHelper mh;

    private Connection conn = null;

    private PreparedStatement pres = null;
    
    public static MemberHelper getHelper() {
    	
        if(mh == null) mh = new MemberHelper();     
        return mh;
    }
    
   
    public boolean checkDuplicate(Member m){   //檢查email有沒有重複
        int row = -1;
        ResultSet rs = null;        
        try {
            conn = DBMgr.getConnection();
            String sql = "SELECT count(*) FROM `ncutix`.`member` WHERE `email` = ?";
            String email = m.getEmail();
            pres = conn.prepareStatement(sql);
            pres.setString(1, email);
            rs = pres.executeQuery();
            rs.next();
            row = rs.getInt("count(*)");

        } 
        catch (SQLException e) 
        {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            DBMgr.close(rs, pres, conn);
        }
       
        return (row == 0) ? false : true;
    }
    
   
    public JSONObject create(Member m) {
        
    	ZoneId zid = ZoneId.of("Asia/Taipei"); 
    	String exexcute_sql = "";
        long start_time = System.nanoTime();
        int row = 0;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            String sql = "INSERT INTO `ncutix`.`member`(`name`, `email`, `passwd`, `dob`, `update_time`)"
                    + " VALUES(?, ?, ?, ?, ?)";
            
            String name = m.getName();
            String email = m.getEmail();
            String passwd = m.getPasswd();
            Date dob = m.getDob();
            
            LocalDateTime lt = LocalDateTime.now(zid); 
            pres = conn.prepareStatement(sql);
            pres.setString(1, name);
            pres.setString(2, email);
            pres.setString(3, passwd);
            pres.setTimestamp(4, new java.sql.Timestamp(dob.getTime()),Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
            pres.setTimestamp(5, Timestamp.valueOf(lt),Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));

            row = pres.executeUpdate();

            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(pres, conn);
        }

        long end_time = System.nanoTime();
        long duration = (end_time - start_time);
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("time", duration);
        response.put("row", row);

        return response;
    }
    
 
    public JSONObject update(Member m) {

        JSONArray jsa = new JSONArray();
        String exexcute_sql = "";
        long start_time = System.nanoTime();
        int row = 0;
        
        try {
           
            conn = DBMgr.getConnection();
            String sql = "Update `ncutix`.`member` SET `name` = ? ,`passwd` = ? ,`dob` = ? , `update_time` = ? WHERE `email` = ?";
            
            String name = m.getName();
            String email = m.getEmail();
            String passwd = m.getPasswd();
            Date dob = m.getDob();
            
            pres = conn.prepareStatement(sql);
            pres.setString(1, name);
            pres.setString(2, passwd);
            pres.setTimestamp(3, new java.sql.Timestamp(dob.getTime()),Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
            pres.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()),Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
            pres.setString(5, email);
           
            row = pres.executeUpdate();

            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(pres, conn);
        }
        
        long end_time = System.nanoTime();
        long duration = (end_time - start_time);
        
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }
 
    
    public JSONObject login(String member_email, String member_passwd) {
    	
        Member m = null;
        JSONArray jsa = new JSONArray();
        String exexcute_sql = "";
        long start_time = System.nanoTime();
        int row = 0;
        ResultSet rs = null;
        
        try {
       
            conn = DBMgr.getConnection();
            String sql = "SELECT * FROM `ncutix`.`member` WHERE `email` = ? AND `passwd` = ? LIMIT 1";
            
            pres = conn.prepareStatement(sql);
            pres.setString(1, member_email);
            pres.setString(2, member_passwd);
            rs = pres.executeQuery();
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            while(rs.next()) {
          
                row += 1;
                int ID = rs.getInt("ID");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String passwd = rs.getString("passwd");
                Date dob = rs.getDate("dob");
                Timestamp update_time = rs.getTimestamp("update_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));              
                m = new Member(ID, name, email, passwd, dob, update_time);
                jsa.put(m.getData());
            }
            
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(rs, pres, conn);
        }
        
        long end_time = System.nanoTime();
        long duration = (end_time - start_time);

        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }
    
    public boolean checklogin(Member m){   //檢查login
        int row=-1;
        ResultSet rs = null;        
        try {
            conn = DBMgr.getConnection();
            String sql = "SELECT count(*) FROM `ncutix`.`member` WHERE `email` = ? AND `passwd` = ?";
            String email = m.getEmail();
            String passwd = m.getPasswd();
            pres = conn.prepareStatement(sql);
            pres.setString(1, email);
            pres.setString(2, passwd);
            rs = pres.executeQuery();
            rs.next();
            row = rs.getInt("count(*)");

        } 
        catch (SQLException e) 
        {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        } 
        finally 
        {
            DBMgr.close(rs, pres, conn);
        }
       
        return (row == 1) ? true : false;
    }
  
    public void updateUpdate_time(Member m) {
     
        Timestamp update_time = m.getUpdate_time();
        String exexcute_sql = "";
        
        try {
            conn = DBMgr.getConnection();
            String sql = "Update `ncutix`.`member` SET `update_time` = ? WHERE `ID` = ?";
            int ID = m.getID();
            
            pres = conn.prepareStatement(sql);
            pres.setTimestamp(1, update_time,Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));
            pres.setInt(2, ID);
            pres.executeUpdate();

            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(pres, conn);
        }
    }
    
    public JSONObject getByID(int member_id) {
        
        Member m = null;
        //JSONArray jsa = new JSONArray();
        JSONObject jso = new JSONObject();
        String exexcute_sql = "";
        long start_time = System.nanoTime();
        int row = 0;
        ResultSet rs = null;
        
        try {
            conn = DBMgr.getConnection();
            String sql = "SELECT * FROM `ncutix`.`member` WHERE ID = ? LIMIT 1";
            pres = conn.prepareStatement(sql);
            pres.setInt(1, member_id);
            rs = pres.executeQuery();
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            while(rs.next()) {
             
                row += 1;                
                int ID = rs.getInt("ID");
                String name = rs.getString("name");
                String email = rs.getString("email");
                String passwd = rs.getString("passwd");
                Date dob = rs.getDate("dob");
                Timestamp update_time = rs.getTimestamp("update_time",Calendar.getInstance(TimeZone.getTimeZone("GMT+8")));              
                m = new Member(ID, name, email, passwd, dob, update_time);
                //jsa.put(m.getData());
                jso = m.getData();
            }
            
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(rs, pres, conn);
        }

        long end_time = System.nanoTime();
        long duration = (end_time - start_time);
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        //response.put("data", jsa);
        response.put("data", jso);

        return response;
    }
    

    
    
}
