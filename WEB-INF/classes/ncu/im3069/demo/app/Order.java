package ncu.im3069.demo.app;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;

import org.json.*;

public class Order {

    private int member_id;
	private int ID;
    private String address;
    private String phone;    
    private String payment_method;
    
    

    /** list，訂單列表 */
    private ArrayList<OrderItem> list = new ArrayList<OrderItem>();   
    
    private Timestamp order_time;
    
    private Timestamp payment_time = null;
    
    private int is_del = 0;

    private OrderItemHelper oph = OrderItemHelper.getHelper();

    public Order(int member_id,String address, String phone, String payment_method) {
    	
    	this.member_id = member_id;
        this.address = address;
        this.phone = phone;
        this.payment_method = payment_method;
        this.order_time = Timestamp.valueOf(LocalDateTime.now());
        this.payment_time = Timestamp.valueOf(LocalDateTime.now());
        this.is_del = 0;
        
    }
    
    public Order(int ID,int member_id, String address, String phone, String payment_method, Timestamp order_time, Timestamp payment_time, int is_del) {
    	
        this.ID = ID;
        this.member_id = member_id;
        this.address = address;
        this.phone = phone;
        this.payment_method = payment_method;
        this.order_time = order_time;
        this.payment_time = payment_time;
        this.is_del = is_del;
        getOrderTicketFromDB();
        
    }
    
    public void addOrderTicket(Ticket ticket) {
        this.list.add(new OrderItem(ticket));
    }

    public void addOrderProduct(OrderItem oi) {
        this.list.add(oi);
    }
    
    public void setId(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return this.ID;
    }

    
    public String getPayment_method()
    {
    	return this.payment_method;
    }

    public Timestamp getOrder_time() {
        return this.order_time;
    }

    public String getAddress() {
        return this.address;
    }

    public String getPhone() {
        return this.phone;
    }
    
    public Timestamp getPayment_time() {
        return this.payment_time;
    }
    
    public int getIs_del() {
        return this.is_del;
    }


    public ArrayList<OrderItem> getOrderTicket() {
        return this.list;
    }
    
    
    private void getOrderTickerFromDB() {
        ArrayList<OrderItem> data = oph.getOrderTicketByOrderId(this.ID);
        this.list = data;
    }
    
   
    public int getMemberID() {
    	return this.member_id;
    }
 
    public JSONObject getOrderData() {
        JSONObject jso = new JSONObject();
        jso.put("ID", getID());
        jso.put("member_id",getMemberID());
        jso.put("address", getAddress());
        jso.put("phone", getPhone());
        jso.put("payment_method", payment_method);
        jso.put("order_time", getOrder_time());
        jso.put("payment_time", getPayment_time());
        jso.put("is_del", getIs_del());
      

        return jso;
    }

    public JSONArray getOrderTicketData() {
        JSONArray result = new JSONArray();

        for(int i=0 ; i < this.list.size() ; i++) {
            result.put(this.list.get(i).getData());
        }

        return result;
    }
    

    public JSONObject getOrderAllInfo() {
        JSONObject jso = new JSONObject();
        jso.put("order_info", getOrderData());
        jso.put("ticket_info", getOrderTicketData());

        return jso;
    }

    public void setOrderTicketID(JSONArray data) {
        for(int i=0 ; i < this.list.size() ; i++) {
            this.list.get(i).setID((int) data.getLong(i));
        }
    }
    
    private void getOrderTicketFromDB() {
        ArrayList<OrderItem> data = oph.getOrderTicketByOrderId(this.ID);
        this.list = data;
    }


}
