package ncu.im3069.demo.app;

import org.json.*;

public class Seatlist {//座位清單
	
	private int ID;
	private boolean status= false;//狀態
	private String seat;//座位	
	private int price;
	
	public Seatlist(int ID, boolean status, String seat, int price) {
		this.ID=ID;
		this.status=status;
		this.seat=seat;
		this.price=price;
	}
	
	public int getID() {
		return this.ID;
	}
	
	public boolean getStatus() {
		return this.status;
	}
	
	public String getSeat() {
		return this.seat;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public JSONObject getData() {
        /** ��SONObject撠府�������銋����脰����*/
        JSONObject jso = new JSONObject();
        jso.put("ID", getID());
        jso.put("status", getStatus());
        jso.put("seat", getSeat());
        jso.put("price", getPrice());

        return jso;
    }

}
