package ncu.im3069.demo.app;

import java.sql.*;
import java.util.*;

import org.json.*;

import ncu.im3069.demo.util.DBMgr;

public class OrderItemHelper {
    
    private OrderItemHelper() {
        
    }
    
    private static OrderItemHelper oph;
    private Connection conn = null;
    private PreparedStatement pres = null;
    
    public static OrderItemHelper getHelper() {
 
        if(oph == null) oph = new OrderItemHelper();        
        return oph;
    }
    
    public JSONArray createByList(long order_id, List<OrderItem> orderticket) {
        JSONArray jsa = new JSONArray();
        String exexcute_sql = "";
        
        for(int i=0 ; i < orderticket.size() ; i++) {
            OrderItem op = orderticket.get(i);

            int ticket_id = op.getTicket().getID();
            int price = op.getPrice();
        
            try {

                conn = DBMgr.getConnection();
                String sql = "INSERT INTO `ncutix`.`order_ticket`(`order_id`, `ticket_id`, `price`)"
                        + " VALUES(?, ?, ?)";

                pres = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                pres.setLong(1, order_id);
                pres.setInt(2, ticket_id);
                pres.setInt(3, price);

                pres.executeUpdate();

                exexcute_sql = pres.toString();
                System.out.println(exexcute_sql);
                
                ResultSet rs = pres.getGeneratedKeys();

                if (rs.next()) {
                    long ID = rs.getLong(1);
                    jsa.put(ID);
                }
            } catch (SQLException e) {
                /** 印出JDBC SQL指令錯誤 **/
                System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
            } catch (Exception e) {
                /** 若錯誤則印出錯誤訊息 */
                e.printStackTrace();
            } finally {
                /** 關閉連線並釋放所有資料庫相關之資源 **/
                DBMgr.close(pres, conn);
            }
        }
        
        return jsa;
    }
    
    public ArrayList<OrderItem> getOrderTicketByOrderId(int order_id) {
        ArrayList<OrderItem> result = new ArrayList<OrderItem>();

        String exexcute_sql = "";
        ResultSet rs = null;
        OrderItem op;
        
        try {
            conn = DBMgr.getConnection();
            String sql = "SELECT * FROM `ncutix`.`order_ticket` WHERE `order_ticket`.`order_id` = ?";

            pres = conn.prepareStatement(sql);
            pres.setInt(1, order_id);

            rs = pres.executeQuery();

            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            while(rs.next()) {
                int ID = rs.getInt("ID");
                int ticket_id = rs.getInt("ticket_id");
                int price = rs.getInt("price");

                op = new OrderItem(ID, order_id, ticket_id, price);
                
                result.add(op);
            }
        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(pres, conn);
        }
        
        return result;
    }
}
