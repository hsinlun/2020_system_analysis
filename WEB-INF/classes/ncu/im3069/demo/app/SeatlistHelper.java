package ncu.im3069.demo.app;

import java.sql.*;
import java.util.Date;

import org.json.*;

import ncu.im3069.demo.util.DBMgr;
import ncu.im3069.demo.app.Seatlist;

public class SeatlistHelper {
	
    private SeatlistHelper() {
        
    }
    
    private static SeatlistHelper slh;
    private Connection conn = null;
    private PreparedStatement pres = null;
    
    public static SeatlistHelper getHelper() {
        if(slh == null) slh = new SeatlistHelper();
        
        return slh;
    }
    
    public JSONObject getAll() {//取出全部seat
    	Seatlist sl = null;
        JSONArray jsa = new JSONArray();
        String exexcute_sql = "";
        long starts_time = System.nanoTime();
        int row = 0;
        ResultSet rs = null;
        
        try {
            conn = DBMgr.getConnection();
            String sql = "SELECT * FROM `ncutix`.`seatlist`";
            
            pres = conn.prepareStatement(sql);
            rs = pres.executeQuery();

            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            while(rs.next()) {
                row += 1;
                
                int seatlist_ID = rs.getInt("ID");
                boolean status = rs.getBoolean("status");
                String seat = rs.getString("seat");
                int price = rs.getInt("price");
                
                sl = new Seatlist(seatlist_ID, status, seat, price);
                jsa.put(sl.getData());
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(rs, pres, conn);
        }
        
        long ends_time = System.nanoTime();
        long duration = (ends_time - starts_time);
        
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }
    
    public JSONObject getByIdList(String data) {//照ID排序取出
      Seatlist sl = null;
      JSONArray jsa = new JSONArray();
      String exexcute_sql = "";
      long starts_time = System.nanoTime();
      int row = 0;
      ResultSet rs = null;

      try {
          conn = DBMgr.getConnection();
          String[] in_para = DBMgr.stringToArray(data, ",");
          String sql = "SELECT * FROM `ncutix`.`seatlist` WHERE `seatlist`.`ID`";
          for (int i=0 ; i < in_para.length ; i++) {
              sql += (i == 0) ? "in (?" : ", ?";
              sql += (i == in_para.length-1) ? ")" : "";
          }
          
          pres = conn.prepareStatement(sql);
          for (int i=0 ; i < in_para.length ; i++) {
            pres.setString(i+1, in_para[i]);
          }
          rs = pres.executeQuery();

          exexcute_sql = pres.toString();
          System.out.println(exexcute_sql);
          
          while(rs.next()) {
              row += 1;
              
              int seatlist_ID = rs.getInt("ID");
              boolean status = rs.getBoolean("status");
              String seat = rs.getString("seat");
              int price = rs.getInt("price");
              
              sl = new Seatlist(seatlist_ID, status, seat, price);
              jsa.put(sl.getData());
          }

      } catch (SQLException e) {
          System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
      } catch (Exception e) {
          e.printStackTrace();
      } finally {
          DBMgr.close(rs, pres, conn);
      }
      
      long ends_time = System.nanoTime();
      long duration = (ends_time - starts_time);
      
      JSONObject response = new JSONObject();
      response.put("sql", exexcute_sql);
      response.put("row", row);
      response.put("time", duration);
      response.put("data", jsa);

      return response;
  }
    
    public Seatlist getById(String ID) {
        Seatlist sl = null;
        String exexcute_sql = "";
        ResultSet rs = null;
        
        try {
            conn = DBMgr.getConnection();
            String sql = "SELECT * FROM `ncutix`.`seatlist` WHERE `seatlist`.`ID` = ? LIMIT 1";
            
            pres = conn.prepareStatement(sql);
            pres.setString(1, ID);
            rs = pres.executeQuery();

            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            while(rs.next()) {
            	int seatlist_ID = rs.getInt("ID");
                boolean status = rs.getBoolean("status");
                String seat = rs.getString("seat");
                int price = rs.getInt("price");
                
                sl = new Seatlist(seatlist_ID, status, seat, price);
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DBMgr.close(rs, pres, conn);
        }

        return sl;
    }
}
