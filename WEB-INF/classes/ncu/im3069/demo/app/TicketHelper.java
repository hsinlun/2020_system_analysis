package ncu.im3069.demo.app;

import java.sql.*;
import java.sql.Date;
import java.util.List;

import org.json.*;

import ncu.im3069.demo.util.DBMgr;
import ncu.im3069.demo.app.Ticket;

public class TicketHelper {
    private TicketHelper() {
        
    }
    
    private static TicketHelper th;
    private Connection conn = null;
    private PreparedStatement pres = null;
    
    public static TicketHelper getHelper() {
        /** Singleton檢查是否已經有ProductHelper物件，若無則new一個，若有則直接回傳 */
        if(th == null) th = new TicketHelper();
        
        return th;
    }
    
    public JSONObject create(Ticket t) {//新增一個ticket
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 紀錄程式開始執行時間 */
        long starts_time = System.nanoTime();
        /** 紀錄SQL總行數 */
        int row = 0;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "INSERT INTO `ncutix`.`ticket`(`title`, `artist`, `price`, `seat`, `start_time`, `end_time`)"
                    + " VALUES(?, ?, ?, ?, ?, ?)";
            
            /** 取得所需之參數 */
            String title = t.getTitle();
            String artist = t.getArtist();
            int price = t.getPrice();
            String seat = t.getSeat();
            String start_time = t.getStarttime();
            String end_time = t.getEndtime();   
            
            /** 將參數回填至SQL指令當中 */
            pres = conn.prepareStatement(sql);
            pres.setString(1, title);
            pres.setString(2, artist);
            pres.setInt(3, price);
            pres.setString(4, seat);
            pres.setString(5, start_time);
            pres.setString(6, end_time);
            
            /** 執行新增之SQL指令並記錄影響之行數 */
            row = pres.executeUpdate();
            
            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(pres, conn);
        }

        /** 紀錄程式結束執行時間 */
        long ends_time = System.nanoTime();
        /** 紀錄程式執行時間 */
        long duration = (ends_time - starts_time);

        /** 將SQL指令、花費時間與影響行數，封裝成JSONObject回傳 */
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("time", duration);
        response.put("row", row);

        return response;
    }
    
    public JSONArray createByList(int ticket_ID,List<Ticket> tickets) {
        JSONArray jsa = new JSONArray();
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        
        for(int i=0 ; i < tickets.size() ; i++) {
            Ticket t = tickets.get(i);
            
            /** 取得所需之參數 */
            int ID = t.getID();
            String title = t.getTitle();
            String artist = t.getArtist();
            int price = t.getPrice();
            String seat = t.getSeat();
            String start_time = t.getStarttime();
            String end_time = t.getEndtime();
            
            try {
                /** 取得資料庫之連線 */
                conn = DBMgr.getConnection();
                /** SQL指令 */
                String sql = "INSERT INTO `ncutix`.`order_ticket`(`ticket_ID`, `title`,`artist`, `price`, `seat`, `start_time`,`end_time`)"
                        + " VALUES(?, ?, ?, ?, ?)";
                
                /** 將參數回填至SQL指令當中 */
                pres = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                pres.setInt(1, ticket_ID);
                pres.setString(2, title);
                pres.setString(3, artist);
                pres.setInt(4, price);
                pres.setString(5, start_time);
                pres.setString(6, end_time);
                
                /** 執行新增之SQL指令並記錄影響之行數 */
                pres.executeUpdate();
                
                /** 紀錄真實執行的SQL指令，並印出 **/
                exexcute_sql = pres.toString();
                System.out.println(exexcute_sql);
                
                ResultSet rs = pres.getGeneratedKeys();

                if (rs.next()) {
                    long lnID = rs.getLong(1);
                    jsa.put(lnID);
                }
            } catch (SQLException e) {
                /** 印出JDBC SQL指令錯誤 **/
                System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
            } catch (Exception e) {
                /** 若錯誤則印出錯誤訊息 */
                e.printStackTrace();
            } finally {
                /** 關閉連線並釋放所有資料庫相關之資源 **/
                DBMgr.close(pres, conn);
            }
        }
        
        return jsa;
    }
    public JSONObject getAll() {
        /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
    	Ticket t = null;
        /** 用於儲存所有檢索回之商品，以JSONArray方式儲存 */
        JSONArray jsa = new JSONArray();
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 紀錄程式開始執行時間 */
        long starts_time = System.nanoTime();
        /** 紀錄SQL總行數 */
        int row = 0;
        /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "SELECT * FROM `ncutix`.`ticket`";
            
            /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
            pres = conn.prepareStatement(sql);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();

            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
            while(rs.next()) {
                /** 每執行一次迴圈表示有一筆資料 */
                row += 1;
                
                /** 將 ResultSet 之資料取出 */
                int ID = rs.getInt("ID");
                String title = rs.getString("title");
                String artist = rs.getString("artist");
                int price = rs.getInt("price");
                String seat = rs.getString("seat");
                String start_time = rs.getString("start_time");
                String end_time = rs.getString("end_time");      
                /** 將每一筆商品資料產生一名新Product物件 */
                t = new Ticket(ID, title, artist, price, seat, start_time, end_time);
                /** 取出該項商品之資料並封裝至 JSONsonArray 內 */
                jsa.put(t.getData());
            }

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }
        
        /** 紀錄程式結束執行時間 */
        long ends_time = System.nanoTime();
        /** 紀錄程式執行時間 */
        long duration = (ends_time - starts_time);
        
        /** 將SQL指令、花費時間、影響行數與所有會員資料之JSONArray，封裝成JSONObject回傳 */
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }
    
    public JSONObject getByIdList(String data) {
      /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
      Ticket t = null;
      /** 用於儲存所有檢索回之商品，以JSONArray方式儲存 */
      JSONArray jsa = new JSONArray();
      /** 記錄實際執行之SQL指令 */
      String exexcute_sql = "";
      /** 紀錄程式開始執行時間 */
      long starts_time = System.nanoTime();
      /** 紀錄SQL總行數 */
      int row = 0;
      /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
      ResultSet rs = null;

      try {
          /** 取得資料庫之連線 */
          conn = DBMgr.getConnection();
          String[] in_para = DBMgr.stringToArray(data, ",");
          /** SQL指令 */
          String sql = "SELECT * FROM `ncutix`.`ticket` WHERE `ticket`.`ID`";
          for (int i=0 ; i < in_para.length ; i++) {
              sql += (i == 0) ? "in (?" : ", ?";
              sql += (i == in_para.length-1) ? ")" : "";
          }
          
          /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
          pres = conn.prepareStatement(sql);
          for (int i=0 ; i < in_para.length ; i++) {
            pres.setString(i+1, in_para[i]);
          }
          /** 執行查詢之SQL指令並記錄其回傳之資料 */
          rs = pres.executeQuery();

          /** 紀錄真實執行的SQL指令，並印出 **/
          exexcute_sql = pres.toString();
          System.out.println(exexcute_sql);
          
          /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
          while(rs.next()) {
              /** 每執行一次迴圈表示有一筆資料 */
              row += 1;
              
              /** 將 ResultSet 之資料取出 */
              int ID = rs.getInt("ID");
              String title = rs.getString("title");
              String artist = rs.getString("artist");
              int price = rs.getInt("price");
              String seat = rs.getString("seat");
              String start_time = rs.getString("start_time");
              String end_time = rs.getString("end_time");   
              /** 將每一筆商品資料產生一名新Product物件 */
              t = new Ticket(ID, title, artist, price, seat, start_time, end_time);
              /** 取出該項商品之資料並封裝至 JSONsonArray 內 */
              jsa.put(t.getData());
          }

      } catch (SQLException e) {
          /** 印出JDBC SQL指令錯誤 **/
          System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
      } catch (Exception e) {
          /** 若錯誤則印出錯誤訊息 */
          e.printStackTrace();
      } finally {
          /** 關閉連線並釋放所有資料庫相關之資源 **/
          DBMgr.close(rs, pres, conn);
      }
      
      /** 紀錄程式結束執行時間 */
      long ends_time = System.nanoTime();
      /** 紀錄程式執行時間 */
      long duration = (ends_time - starts_time);
      
      /** 將SQL指令、花費時間、影響行數與所有會員資料之JSONArray，封裝成JSONObject回傳 */
      JSONObject response = new JSONObject();
      response.put("sql", exexcute_sql);
      response.put("row", row);
      response.put("time", duration);
      response.put("data", jsa);

      return response;
  }
    
    public Ticket getById(String id) {
        /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
        Ticket t = null;
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "SELECT * FROM `ncutix`.`ticket` WHERE `ticket`.`ID` = ? LIMIT 1";
            
            /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
            pres = conn.prepareStatement(sql);
            pres.setString(1, id);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();

            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
            while(rs.next()) {
                /** 將 ResultSet 之資料取出 */
            	int ID = rs.getInt("ID");
                String title = rs.getString("title");
                String artist = rs.getString("artist");
                int price = rs.getInt("price");
                String seat = rs.getString("seat");
                String start_time = rs.getString("start_time");
                String end_time = rs.getString("end_time");   
                 
                /** 將每一筆商品資料產生一名新Product物件 */
                t = new Ticket(ID, title, artist, price, seat, start_time, end_time);
            }

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }

        return t;
    }// end getByID
    
    public boolean getByTitleandSeatName(String acti_title, String seat_name) {
        /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
        Ticket t = null;
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            //String sql = "SELECT * FROM `ncutix`.`ticket` WHERE `ticket`.`ID` = ? LIMIT 1";
            String sql = "SELECT * FROM `ncutix`.`ticket` WHERE `ticket`.`title` = '"+acti_title+"' AND `ticket`.`seat` = '"+seat_name+"' LIMIT 1";
            //System.out.println(sql);
            /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
            pres = conn.prepareStatement(sql);
            //pres.setString(1, id);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();

            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
            while(rs.next()) {
                /** 將 ResultSet 之資料取出 */
            	int ID = rs.getInt("ID");
                String title = rs.getString("title");
                String artist = rs.getString("artist");
                int price = rs.getInt("price");
                String seat = rs.getString("seat");
                String start_time = rs.getString("start_time");
                String end_time = rs.getString("end_time");   
                 
                /** 將每一筆商品資料產生一名新Product物件 */
                t = new Ticket(ID, title, artist, price, seat, start_time, end_time);
            }

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }

        if(t != null) {
        	return true;
        }else {
        	return false;
        }
    }// end getByTitleandSeatName
    
    public JSONObject checkDuplicate(String ac_title) {
        /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
     Ticket t = null;
        /** 用於儲存所有檢索回之商品，以JSONArray方式儲存 */
        JSONArray jsa = new JSONArray();
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 紀錄程式開始執行時間 */
        long starts_time = System.nanoTime();
        /** 紀錄SQL總行數 */
        int row = 0;
        /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "SELECT * FROM `ncutix`.`ticket` where `ticket`.`title`=?";
            
            /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
            pres = conn.prepareStatement(sql);
            pres.setString(1,ac_title);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();

            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
            while(rs.next()) {
                /** 每執行一次迴圈表示有一筆資料 */
                row += 1;
                
                /** 將 ResultSet 之資料取出 */
                int ID = rs.getInt("ID");
                String title = rs.getString("title");
                String artist = rs.getString("artist");
                int price = rs.getInt("price");
                String seat = rs.getString("seat");
                String start_time = rs.getString("start_time");
                String end_time = rs.getString("end_time");      
                /** 將每一筆商品資料產生一名新Product物件 */
                t = new Ticket(ID, title, artist, price, seat, start_time, end_time);
                /** 取出該項商品之資料並封裝至 JSONsonArray 內 */
                jsa.put(t.getData());
            }

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }
        
        /** 紀錄程式結束執行時間 */
        long ends_time = System.nanoTime();
        /** 紀錄程式執行時間 */
        long duration = (ends_time - starts_time);
        
        /** 將SQL指令、花費時間、影響行數與所有會員資料之JSONArray，封裝成JSONObject回傳 */
        JSONObject response = new JSONObject();
        response.put("sql", exexcute_sql);
        response.put("row", row);
        response.put("time", duration);
        response.put("data", jsa);

        return response;
    }//end checkDuplicate
    
    public int checkDuplicateSeat(String ac_title,String seatt) {
        /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
        Ticket t = null;
        /** 用於儲存所有檢索回之商品，以JSONArray方式儲存 */
        JSONArray jsa = new JSONArray();
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 紀錄程式開始執行時間 */
        long starts_time = System.nanoTime();
        /** 紀錄SQL總行數 */
        int row = 0;
        /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "SELECT * FROM `ncutix`.`ticket` where `ticket`.`title`=? and `ticket`.`seat`=?";
           
            
            /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
            pres = conn.prepareStatement(sql);
            pres.setString(1,ac_title);
            pres.setString(2, seatt);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();

            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
            while(rs.next()) {
                /** 每執行一次迴圈表示有一筆資料 */
                row += 1;
                
                /** 將 ResultSet 之資料取出 */
                //int ID = rs.getInt("ID");
//                String title = rs.getString("title");
//                String artist = rs.getString("artist");
//                int price = rs.getInt("price");
//                String seat = rs.getString("seat");
//                String start_time = rs.getString("start_time");
//                String end_time = rs.getString("end_time");      
//                /** 將每一筆商品資料產生一名新Product物件 */
//                t = new Ticket( title, artist, price, seat, start_time, end_time);
//                /** 取出該項商品之資料並封裝至 JSONsonArray 內 */
//                jsa.put(t.getData());
            }

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }
        
        /** 紀錄程式結束執行時間 */
        long ends_time = System.nanoTime();
        /** 紀錄程式執行時間 */
        long duration = (ends_time - starts_time);
        
        /** 將SQL指令、花費時間、影響行數與所有會員資料之JSONArray，封裝成JSONObject回傳 */
//        JSONObject response = new JSONObject();
//        response.put("sql", exexcute_sql);
//        response.put("row", row);
//        response.put("time", duration);
//        response.put("data", jsa);
//
//        return response;
        return row;
    }//end checkDuplicate 
    
    public void deleteById(String title,String seat) {
    	try {
    		conn = DBMgr.getConnection();
    		//String[] in_para = DBMgr.stringToArray(data, ",");
    		String sql = "DELETE FROM `ncutix`.`ticket` WHERE `ticket`.`title`='"+title+"' AND `ticket`.`seat`='"+seat+"'";
//    		String sql = "DELETE * FROM `ncutix`.`ticket` WHERE `ticket`.`title`='"+title+"' and `seat`='"+seat+"'";
    		//String sql = "DELETE * FROM `ncutix`.`ticket` WHERE `ticket`.`title`=? and `seat`=?";
    		pres = conn.prepareStatement(sql);
            //pres.setString(1, title);
            //pres.setString(2, seat);
            System.out.println(sql);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            pres.execute();
            //pres.executeQuery();
            //pres.executeUpdate();
    	}catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(pres, conn);
        }
    }
    
    public Ticket getTicketID(String get_title, String get_seat) {
        /** 新建一個 Product 物件之 m 變數，用於紀錄每一位查詢回之商品資料 */
        Ticket t = null;
        /** 記錄實際執行之SQL指令 */
        String exexcute_sql = "";
        /** 儲存JDBC檢索資料庫後回傳之結果，以 pointer 方式移動到下一筆資料 */
        ResultSet rs = null;
        
        try {
            /** 取得資料庫之連線 */
            conn = DBMgr.getConnection();
            /** SQL指令 */
            String sql = "SELECT * FROM `ncutix`.`ticket` WHERE `ticket`.`title` = ? AND `ticket`.`seat` = ?";
            
            /** 將參數回填至SQL指令當中，若無則不用只需要執行 prepareStatement */
            pres = conn.prepareStatement(sql);
            pres.setString(1, get_title);
            pres.setString(2, get_seat);
            /** 執行查詢之SQL指令並記錄其回傳之資料 */
            rs = pres.executeQuery();

            /** 紀錄真實執行的SQL指令，並印出 **/
            exexcute_sql = pres.toString();
            System.out.println(exexcute_sql);
            
            /** 透過 while 迴圈移動pointer，取得每一筆回傳資料 */
            while(rs.next()) {
                /** 將 ResultSet 之資料取出 */
            	int ID = rs.getInt("ID");
                String title = rs.getString("title");
                String artist = rs.getString("artist");
                int price = rs.getInt("price");
                String seat = rs.getString("seat");
                String start_time = rs.getString("start_time");
                String end_time = rs.getString("end_time");   
                 
                /** 將每一筆商品資料產生一名新Product物件 */
                t = new Ticket(ID, title, artist, price, seat, start_time, end_time);
            }

        } catch (SQLException e) {
            /** 印出JDBC SQL指令錯誤 **/
            System.err.format("SQL State: %s\n%s\n%s", e.getErrorCode(), e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            /** 若錯誤則印出錯誤訊息 */
            e.printStackTrace();
        } finally {
            /** 關閉連線並釋放所有資料庫相關之資源 **/
            DBMgr.close(rs, pres, conn);
        }

        return t;
    }// end getTicketID
    
}//end class
