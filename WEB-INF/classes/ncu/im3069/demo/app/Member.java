package ncu.im3069.demo.app;

import org.json.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.*;
import java.text.*;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class Member {
    
    
    private int ID;
    private String email;
    private String name;
    private String passwd;
    private Timestamp update_time;
    private Date dob;

    private MemberHelper mh =  MemberHelper.getHelper();
    
    
    public Member(String name, String email, String passwd, Date dob) {//產生新會員
    	this.name = name;
    	this.email = email;
        this.passwd = passwd;
        this.dob = dob;
        update();
    }

  
    public Member(int ID,String name, String email, String passwd,  Date dob) {//更新時間
        this.ID = ID;
        this.name = name;
        this.email = email;
        this.passwd = passwd;
        this.dob = dob;
        this.update_time=Timestamp.valueOf(LocalDateTime.now());      
        
    }
    
    public Member(String name, String email, String passwd,  Date dob, Timestamp update_time) {//查詢會員資料
        this.name = name;
        this.email = email;
        this.passwd = passwd;
        this.dob = dob;
        this.update_time= update_time;      
        
    }
    
    public Member(int ID,String name, String email, String passwd,  Date dob, Timestamp update_time) {//查詢會員資料
        this.ID = ID;
    	this.name = name;
        this.email = email;
        this.passwd = passwd;
        this.dob = dob;
        this.update_time= update_time;      
        
    }
    
    public Member(int ID, String name, String passwd,  Date dob) {//更新會員資料
        this.ID = ID;
    	this.name = name;
        this.passwd = passwd;
        this.dob = dob;
        update();      
        
    }
    
    public Member(String email, String passwd) { //登入      
    	this.email = email;
        this.passwd = passwd;        
    }
    
    
   
    public int getID() 
    {
        return this.ID;
    }
    
    public String getName() 
    {
        return this.name;
    }

   
    public String getEmail() 
    {
        return this.email;
    }
 
  
    public String getPasswd() 
    {
        return this.passwd;
    }
    
    public Date getDob() 
    {
        return this.dob;
    }
 
    public Timestamp getUpdate_time() 
    {
        return this.update_time;
    }
  
    
    public JSONObject update() { //更新時間
    	
        JSONObject data = new JSONObject();
        ZoneId zid = ZoneId.of("Asia/Taipei"); 
        LocalDateTime lt = LocalDateTime.now(zid); 
        this.update_time = Timestamp.valueOf(lt);
        if(this.ID != 0) {
            
            mh.updateUpdate_time(this);
            data = mh.update(this);
        }
        
        return data;
    }
    
   
    public JSONObject getData() {
        
        JSONObject jso = new JSONObject();
        jso.put("ID", getID());
        jso.put("name", getName());
        jso.put("email", getEmail());
        jso.put("passwd", getPasswd());
        jso.put("dob", getDob());
        jso.put("update_time", getUpdate_time());
        
        
        return jso;
    }
    
   
    
}