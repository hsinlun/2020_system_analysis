-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- 主機： 127.0.0.1
-- 產生時間： 2021-01-08 15:07:06
-- 伺服器版本： 10.4.14-MariaDB
-- PHP 版本： 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `ncutix`
--

-- --------------------------------------------------------

--
-- 資料表結構 `activity`
--

CREATE TABLE `activity` (
  `ID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `artist` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `activity`
--

INSERT INTO `activity` (`ID`, `title`, `artist`, `description`, `image`, `start_time`, `end_time`) VALUES
(1, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', '不停地嗜愛，不停的是愛\r\n也好久沒有，不停地LIVE\r\n\r\n久等的嗜愛動物，久違的LIVE演出\r\nLOVEHOLIC ，帶著這份壓抑太久的愛\r\n一起成為重回現場的LIVEHOLIC\r\n', '1.jpg', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(2, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', '現象級跨界樂團 七月半 搖滾引爆2020年末!\r\n成軍三年終於等到首場專場演唱會\r\n1205夜露思苦，請多指教!', '2.jpg', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(3, '給世界最悠長的吻 演唱會', 'TANYA', '這個世界因為有每個獨特的小宇宙才如此美麗，灌溉你的小宇宙，讓你像樹一樣伸展你的根向大地，張開你的枝葉向天空。不要失去探索的勇氣，這個世界或許有時候狂風暴雨，打擊或衝撞你，但你沒有發現嗎？他也會擁抱和陪伴你。請享受這場奇妙的雙人舞，用你獨有的舞姿和世界互動，當你越懂得欣賞自己小宇宙裡的萬千變化，就越能看見這個大世界反射出你美麗的各種色彩。', '3.jpg', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(4, '日環食Eclipse 演唱會', '邱鋒澤', '面向如此浩瀚，允許我還是想起了你，隱匿最深的，往往在我安靜下來的片刻，一覽無遺。\r\n\r\n那時候的傍晚無論從哪個方向看去，都不同於現在的傍晚，而我像是困在眼底的光暈，只能在不同角度尋找著你的身影。\r\n\r\n於是虛擬的歌詞產生真實的眼淚，小心拿捏地唱著，想悄悄地把流失的傷害彌補起來，就這樣徒勞無功迎接全部的黑暗。\r\n\r\n經過了靜音滑動的光影，人們又驚又喜的嘆息落了滿地，那些曾經盛開的花草使勁全力也無法重回深根，隨著結束走出你的院子，便成了十足的陌生，從此，誰又將面臨時間的缺席。', '4.png', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(5, '明天留給我 演唱會', '宇宙人', '揮別悶壞了的心情 Fly Away\r\n直達零距離的心裡 Fly to You\r\n\r\n現在就Check In！\r\n宇宙人和你一言為定：\r\n明天留給我，不見不散！', '5.png', '2021-01-23 19:00:00', '2021-01-23 23:00:00');

-- --------------------------------------------------------

--
-- 資料表結構 `member`
--

CREATE TABLE `member` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `passwd` varchar(255) NOT NULL,
  `dob` date NOT NULL,
  `update_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `member`
--

INSERT INTO `member` (`ID`, `name`, `email`, `passwd`, `dob`, `update_time`) VALUES
(2, 'lucy', 'test0105@tmail.com', 'x1234567', '2021-01-03', '2021-01-08 14:13:12'),
(3, 'yytytyt', '0105test@tmail.com', 't1234567', '2020-11-09', '2021-01-05 12:05:50'),
(4, 'yytytyt', '0101215test@tmail.com', 't1234567', '2020-11-09', '2021-01-05 12:06:37'),
(5, 'NONO NO', 'admin@gmail.com', 't1234567', '2020-11-11', '2021-01-05 12:06:53'),
(6, 'tewt', 'asd123@gmail.com', 'g1234567', '2021-01-02', '2021-01-05 12:08:10'),
(7, '鈺凡', 'abc@aa.tw', 'q12121212', '1999-08-14', '2021-01-08 14:42:03'),
(8, 'yes', 'uu@gamil.com', 'aa1234567890', '2021-01-02', '2021-01-08 14:55:57');

-- --------------------------------------------------------

--
-- 資料表結構 `order`
--

CREATE TABLE `order` (
  `ID` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `order_time` datetime NOT NULL,
  `payment_time` datetime NOT NULL,
  `is_del` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `order`
--

INSERT INTO `order` (`ID`, `member_id`, `address`, `phone`, `payment_method`, `order_time`, `payment_time`, `is_del`) VALUES
(1, 2, '', '', 'Choose...', '2021-01-07 13:33:00', '2021-01-07 13:33:00', 0),
(2, 2, 'NO', '035712858', 'credit_card', '2021-01-08 00:03:09', '2021-01-08 00:03:09', 0),
(3, 2, '中大路300號', '0928206836', 'credit_card', '2021-01-08 00:04:32', '2021-01-08 00:04:32', 0),
(4, 2, '中大路300號', '0928206836', 'credit_card', '2021-01-08 14:16:31', '2021-01-08 14:16:31', 0),
(5, 2, '中大路300號', '0928206836', 'Choose...', '2021-01-08 14:25:01', '2021-01-08 14:25:01', 0),
(6, 2, '中大路300號', '0928206836', 'credit_card', '2021-01-08 14:53:25', '2021-01-08 14:53:25', 0),
(7, 2, '中央路232巷32號', '0928206836', 'atm', '2021-01-08 15:04:00', '2021-01-08 15:04:00', 0),
(8, 2, 'ss', 'ss', 'credit_card', '2021-01-08 16:33:14', '2021-01-08 16:33:14', 0);

-- --------------------------------------------------------

--
-- 資料表結構 `order_ticket`
--

CREATE TABLE `order_ticket` (
  `ID` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `order_ticket`
--

INSERT INTO `order_ticket` (`ID`, `order_id`, `ticket_id`, `price`) VALUES
(1, 2, 58, 3000),
(2, 3, 59, 3000),
(3, 3, 60, 3000),
(4, 4, 64, 2000),
(5, 5, 65, 3000),
(6, 6, 67, 2000),
(7, 7, 68, 4000),
(8, 7, 69, 4000),
(9, 8, 99, 2000),
(10, 8, 100, 2000);

-- --------------------------------------------------------

--
-- 資料表結構 `seatlist`
--

CREATE TABLE `seatlist` (
  `ID` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `seat` varchar(255) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `seatlist`
--

INSERT INTO `seatlist` (`ID`, `status`, `seat`, `price`) VALUES
(1, 0, 'A1', 4000),
(2, 0, 'A2', 4000),
(3, 0, 'A3', 4000),
(4, 0, 'A4', 4000),
(5, 0, 'A5', 4000),
(6, 0, 'A6', 4000),
(7, 0, 'A7', 4000),
(8, 0, 'A8', 4000),
(9, 0, 'A9', 4000),
(10, 0, 'A10', 4000),
(11, 0, 'B1', 3000),
(12, 0, 'B2', 3000),
(13, 0, 'B3', 3000),
(14, 0, 'B4', 3000),
(15, 0, 'B5', 3000),
(16, 0, 'B6', 3000),
(17, 0, 'B7', 3000),
(18, 0, 'B8', 3000),
(19, 0, 'B9', 3000),
(20, 0, 'B10', 3000),
(21, 0, 'B11', 3000),
(22, 0, 'B12', 3000),
(23, 0, 'C1', 2000),
(24, 0, 'C2', 2000),
(25, 0, 'C3', 2000),
(26, 0, 'C4', 2000),
(27, 0, 'C5', 2000),
(28, 0, 'C6', 2000),
(29, 0, 'C7', 2000),
(30, 0, 'C8', 2000),
(31, 0, 'C9', 2000),
(32, 0, 'C10', 2000),
(33, 0, 'C11', 2000),
(34, 0, 'C12', 2000),
(35, 0, 'D1', 1000),
(36, 0, 'D2', 1000),
(37, 0, 'D3', 1000),
(38, 0, 'D4', 1000),
(39, 0, 'D5', 1000),
(40, 0, 'D6', 1000),
(41, 0, 'D7', 1000),
(42, 0, 'D8', 1000),
(43, 0, 'D9', 1000),
(44, 0, 'D10', 1000),
(45, 0, 'D11', 1000),
(46, 0, 'D12', 1000);

-- --------------------------------------------------------

--
-- 資料表結構 `ticket`
--

CREATE TABLE `ticket` (
  `ID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `artist` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `seat` varchar(255) NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 傾印資料表的資料 `ticket`
--

INSERT INTO `ticket` (`ID`, `title`, `artist`, `price`, `seat`, `start_time`, `end_time`) VALUES
(46, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D7', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(47, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D7', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(48, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 4000, 'A1', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(49, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 4000, 'A2', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(50, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 4000, 'A3', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(51, '給世界最悠長的吻 演唱會', 'TANYA', 4000, 'A2', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(52, '給世界最悠長的吻 演唱會', 'TANYA', 4000, 'A4', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(53, '給世界最悠長的吻 演唱會', 'TANYA', 4000, 'A3', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(54, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B2', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(55, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 2000, 'C2', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(56, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 1000, 'D2', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(57, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 3000, 'B1', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(58, '給世界最悠長的吻 演唱會', 'TANYA', 3000, 'B6', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(59, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 3000, 'B5', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(60, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 3000, 'B6', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(61, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 4000, 'A5', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(62, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 3000, 'B3', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(63, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 4000, 'A8', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(64, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 2000, 'C8', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(65, '給世界最悠長的吻 演唱會', 'TANYA', 3000, 'B2', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(66, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D10', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(67, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 2000, 'C5', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(68, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 4000, 'A1', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(69, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 4000, 'A2', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(70, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D2', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(71, '明天留給我 演唱會', '宇宙人', 4000, 'A3', '2021-01-23 19:00:00', '2021-01-23 23:00:00'),
(72, '明天留給我 演唱會', '宇宙人', 4000, 'A4', '2021-01-23 19:00:00', '2021-01-23 23:00:00'),
(73, '明天留給我 演唱會', '宇宙人', 4000, 'A4', '2021-01-23 19:00:00', '2021-01-23 23:00:00'),
(74, '明天留給我 演唱會', '宇宙人', 4000, 'A6', '2021-01-23 19:00:00', '2021-01-23 23:00:00'),
(75, '明天留給我 演唱會', '宇宙人', 4000, 'A6', '2021-01-23 19:00:00', '2021-01-23 23:00:00'),
(79, '日環食Eclipse 演唱會', '邱鋒澤', 4000, 'A1', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(82, '日環食Eclipse 演唱會', '邱鋒澤', 4000, 'A4', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(83, '日環食Eclipse 演唱會', '邱鋒澤', 4000, 'A5', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(84, '日環食Eclipse 演唱會', '邱鋒澤', 4000, 'A6', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(85, '日環食Eclipse 演唱會', '邱鋒澤', 3000, 'B6', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(86, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 4000, 'A5', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(87, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B5', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(88, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B6', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(89, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 4000, 'A4', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(90, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B5', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(91, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B6', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(92, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B7', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(93, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 3000, 'B5', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(94, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 1000, 'D6', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(95, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 2000, 'C7', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(96, '夜露思苦yoroshiku 演唱會', '七月半SEVEN FAT', 1000, 'D7', '2021-01-25 17:00:00', '2021-01-25 22:00:00'),
(99, '日環食Eclipse 演唱會', '邱鋒澤', 2000, 'C6', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(100, '日環食Eclipse 演唱會', '邱鋒澤', 2000, 'C7', '2021-01-30 19:00:00', '2021-01-30 23:00:00'),
(101, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 2000, 'C3', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(102, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D4', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(103, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D6', '2021-01-01 18:00:00', '2021-01-01 23:00:00'),
(104, 'LOVEHOLIC嗜愛動物 演唱會', '麋先生MIXER', 1000, 'D5', '2021-01-01 18:00:00', '2021-01-01 23:00:00');

--
-- 已傾印資料表的索引
--

--
-- 資料表索引 `activity`
--
ALTER TABLE `activity`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`ID`) USING BTREE,
  ADD UNIQUE KEY `UNIQUE` (`email`);

--
-- 資料表索引 `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`ID`) USING BTREE;

--
-- 資料表索引 `order_ticket`
--
ALTER TABLE `order_ticket`
  ADD PRIMARY KEY (`ID`);

--
-- 資料表索引 `seatlist`
--
ALTER TABLE `seatlist`
  ADD PRIMARY KEY (`ID`) USING BTREE,
  ADD UNIQUE KEY `UNIQUE` (`seat`);

--
-- 資料表索引 `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ID`) USING BTREE;

--
-- 在傾印的資料表使用自動遞增(AUTO_INCREMENT)
--

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `activity`
--
ALTER TABLE `activity`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `member`
--
ALTER TABLE `member`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `order`
--
ALTER TABLE `order`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `order_ticket`
--
ALTER TABLE `order_ticket`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `seatlist`
--
ALTER TABLE `seatlist`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- 使用資料表自動遞增(AUTO_INCREMENT) `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
